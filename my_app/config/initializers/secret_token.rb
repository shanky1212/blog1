# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
MyApp::Application.config.secret_key_base = 'c38b222e8bbf6a090e0fe1c31d90d0ae38349f934e93b3520e5eceec32e900d93f7a745c63260d1a4e6afe3b8fbb4d11d7f69e09f13a4874a7b77a0a3af821ef'
